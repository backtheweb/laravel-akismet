<?php
namespace Backtheweb\Akismet\Console;

use Backtheweb\Akismet\Facades\Akismet;
use Illuminate\Console\Command;

class AkismetCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'akismet';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Akismet command';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
       //$response = Akismet::verifyKey(config('akismet.key'), config('akismet.blog.name'));
       //dump($response);

       //$akismet = Akismet::instance(config('akismet.key'), config('akismet.blog'));

        $faker = \Faker\Factory::create();

        $data = [
            'user_ip'                => $faker->ipv4,
            'user_agent'             => $faker->userAgent,
            'comment_author'         => 'viagra-test-123', //$faker->name,
            'comment_author_email'   => 'akismet-guaranteed-spam@example.com', //$faker->email,
            'comment_content'        => $faker->text(100),
            'comment_date_gmt'       => $faker->dateTimeAD->format('Y-m-d H:i:s'),
            'is_test'                => 1
        ];


        $response = Akismet::isSpam($data);

        dd($response);



        return Command::SUCCESS;
    }
}
