<?php

namespace Backtheweb\Akismet;

use Backtheweb\Akismet\Console\AkismetCommand;
use Illuminate\Support\ServiceProvider;

class AkismetServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([__DIR__ . '/../config/akismet.php' => config_path('akismet.php')],   'config');

        if ($this->app->runningInConsole()) {
            $this->commands([
                AkismetCommand::class,
            ]);
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/akismet.php', 'akismet');

        //dd(config('akismet'));

        $this->app->bind('akismet', function($app) {

            $key  = $app['config']->get('akismet.key');
            $blog = $app['config']->get('akismet.blog');

            return Akismet::instance($key, $blog);
        });
    }

    public function provides()
    {
        return ['akismet'];
    }

}
