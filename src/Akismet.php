<?php
namespace Backtheweb\Akismet;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Request;

use Backtheweb\Akismet\Exception\CredentialsException;

class Akismet {

    const API_URL = 'https://rest.akismet.com/1.1/';

    protected Client $client;

    /**
     * @param string $key
     * @param string|Blog $blog
     */
    public function __construct(
        protected string $key,
        protected string|Blog $blog,
    )
    {
        $this->client = new Client([
            'base_uri' => self::API_URL,
            'timeout'  => 2.0,
        ]);
    }

    /**
     * @param string $key
     * @param string $blog
     * @return bool
     * @throws GuzzleException
     */
    public static function verifyKey(string $key, string $blog) : bool
    {
        $request = new Request('POST', self::API_URL . 'verify-key');
        $client  = new Client();
        $response   = $client->send($request, ['form_params' => [
            'key'   => $key,
            'blog'  => $blog,
        ]]);

        return $response->getBody()->getContents() === 'valid';
    }

    /**
     * @throws GuzzleException
     */
    public static function instance(string $key, string|array|Blog $blog, string $lang = 'en', string $charset = 'UTF-8') : self
    {
        /** @var Blog $blog */
        $blog = match(true)
        {
            is_string($blog) => new Blog($blog,         $lang,         $charset),
            is_array($blog)  => new Blog($blog['name'], $blog['lang'], $blog['charset']),
            default          => $blog
        };

        if(self::verifyKey($key, $blog->name) !== true) {
            throw new CredentialsException;
        }

        return new self($key, $blog);
    }

    public function verify()
    {
        return self::verifyKey($this->key, $this->blog->name);
    }

    public function __call($name, $arguments)
    {
        if(!method_exists($this, $name)) {

            $action = match ($name) {
                'check'      => 'comment-check',
                'submitSpam' => 'submit-spam',
                'submitHam'  => 'submit-ham',
            };

            return $this->execute($action, $arguments[0]);
        }
    }

    /**
     * @throws GuzzleException
     * @throws \Exception
     */
    private function execute(string $action, array $data = [])
    {
        $data = array_merge([
            'key'                       => $this->key,
            'blog'                      => $this->blog->name,
            'blog_lang'                 => $this->blog->lang,
            'blog_charset'              => $this->blog->charset,
            'is_test'                   => 0,
            'user_ip'                   => null,
            'user_agent'                => null,
            'referrer'                  => null,
            'permalink'                 => null,
            'comment_type'              => 'message',
            'comment_author'            => null,
            'comment_author_email'      => null,
            'comment_author_url'        => null,
            'comment_content'           => null,
            'comment_date_gmt'          => null,
            'comment_post_modified_gmt' => null,

        ], $data);

        $response = $this->client->post($action, [
            'form_params' => $data
        ]);

        $error = $response->getHeader('X-akismet-alert-code');

        if($error) {
            throw new \Exception( config('akismet.exceptions.' . $error, 'Unknown error') , $error);
        }

        $output = $response->getBody()->getContents();

        return $output === "Thanks for making the web a better place." || $output === "true";
    }
}
