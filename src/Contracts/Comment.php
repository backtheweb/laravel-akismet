<?php

namespace Backtheweb\Akismet\Contracts;

use Backtheweb\Akismet\Enums\Type;

interface Comment
{

// 'blog'
// 'user_ip'
// 'user_agent'
// 'referrer'
// 'permalink'
// 'comment_type'
// 'comment_author'
// 'comment_author_email'
// 'comment_author_url'
// 'comment_content'

// 'blog_lang '
// 'is_test'
// 'blog_charset'
// 'comment_date_gmt'
// 'comment_post_modified_gmt'

    public function getAuthor(): string;

    public function getAuthorEmail(): string;

    public function getAuthorUrl(): string;

    public function getContent(): string;

    public function getType(): Type;

    public function getPermalink(): string;

    public function getReferrer(): string;

    public function getUserIp(): string;

    public function getUserAgent(): string;

    public function getCommentType(): string;

    public function getCommentAuthor(): string;

    public function getCommentAuthorEmail(): string;

    public function getCommentAuthorUrl(): string;

    public function getCommentContent(): string;

    public function getCommentDateGmt(): string;

    public function getCommentPostModifiedGmt(): string;

    public function getCommentParent(): string;

    public function getBlog(): string;

    public function getBlogLang(): string;

    public function getBlogCharset(): string;

    public function getUserRole(): string;
}
