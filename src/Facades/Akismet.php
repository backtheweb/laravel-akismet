<?php

namespace Backtheweb\Akismet\Facades;

use Backtheweb\Akismet\Blog;

/**
 * Class Akismet
 * @package Backtheweb\Akismet\Facades
 *
 * @method static instance(string $key, Blog|array|string $blog): Akismet
 * @method static verify(): bool
 * @method static check(array $data = [] ): bool
 * @method static submitSpam(array $data = [] ): bool
 * @method static submitHam(array $data = [] ): bool
 */
class Akismet extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'akismet';
    }
}
