<?php

namespace Backtheweb\Akismet\Enums;

enum Type: string
{
    case COMMENT      = 'comment';
    case REPLY        = 'reply';
    case REVIEW       = 'review';
    case CONTACT_FORM = 'contact-form';
    case SIGNUP       = 'signup';
    case MESSAGE      = 'message';
}
