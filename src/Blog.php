<?php
namespace Backtheweb\Akismet;

class Blog
{
    public function __construct(
        public string $name,
        public string $lang = 'en',
        public string $charset = 'UTF-8',
    ) {

    }
}
