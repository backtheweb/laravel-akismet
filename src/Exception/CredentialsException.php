<?php

namespace Backtheweb\Akismet\Exception;

class CredentialsException extends Exception
{
    protected $message = 'Akismet invalid credentials';
}
