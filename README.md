# Laravel Akismet

[![Latest Version on Packagist](https://img.shields.io/packagist/v/backtheweb/laravel-akismet.svg?style=flat-square)](https://packagist.org/packages/backtheweb/laravel-akismet)
[![Total Downloads](https://img.shields.io/packagist/dt/backtheweb/laravel-akismet.svg?style=flat-square)](https://packagist.org/packages/backtheweb/laravel-akismet)

    $ php artisan vendor:publish  --provider="Backtheweb\Akismet\AkismetServiceProvider" --tag="config"
