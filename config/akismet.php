<?php

return [
    'key'   => env('AKISMET_API_KEY'),
    'blog'  => [
        'name'    => env('AKISMET_BLOG'),
        'lang'    => env('AKISMET_BLOG_LANG', 'en'),
        'charset' => env('AKISMET_BLOG_CHARSET', 'UTF-8')
    ],

    'exceptions' => [
        10001 => 'Your site is using an expired Yahoo! Small Business API key.',
        10003 => 'You must upgrade your Personal subscription to continue using Akismet.',
        10005 => 'Your Akismet API key may be in use by someone else.',
        10006 => 'Your subscription has been suspended due to improper use.',
        10007 => 'Your Akismet API key is being used on more sites than is allowed.',
        10008 => 'Your Akismet API key is being used on more sites than is allowed.',
        10009 => 'Your subscription has been suspended due to overuse.',
        10010 => 'Your subscription has been suspended due to inappropriate use.',
        10011 => 'Your subscription needs to be upgraded due to high usage.',
        10402 => 'Your API key was suspended for non-payment.',
        10403 => 'The owner of your API key has revoked your site\'s access to the key.',
        10404 => 'Your site was not found in the list of sites allowed to use the API key you used.',
        30001 => 'Your Personal subscription needs to be upgraded based on your usage.',
    ]
];
