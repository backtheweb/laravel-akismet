<?php

namespace Backtheweb\Akismet\Tests;

use Backtheweb\Akismet\AkismetServiceProvider;
use Illuminate\Foundation\Bootstrap\LoadEnvironmentVariables;
class TestCase extends \Orchestra\Testbench\TestCase
{
    protected $loadEnvironmentVariables = true;

    public function setUp(): void
    {
        parent::setUp();
        // additional setup
    }

    protected function getPackageProviders($app)
    {
        return [
            AkismetServiceProvider::class,
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        // perform environment setup
        $app['config']->set('akismet.key',          env('AKISMET_API_KEY'));
        $app['config']->set('akismet.blog.name',    env('AKISMET_BLOG'));
        $app['config']->set('akismet.blog.lang',    env('AKISMET_BLOG_LANG'));
        $app['config']->set('akismet.blog.charset', env('AKISMET_BLOG_CHARSET'));
    }

    protected function defineEnvironment($app)
    {
        $app->useEnvironmentPath(__DIR__ . '/../');
        $app->bootstrapWith([LoadEnvironmentVariables::class]);
        parent::defineEnvironment($app);
    }
}
