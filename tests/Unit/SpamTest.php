<?php

namespace Backtheweb\Akismet\Tests\Unit;

use Backtheweb\Akismet\Facades\Akismet;
use Backtheweb\Akismet\Tests\TestCase;

class SpamTest extends TestCase
{

    /** @test **/
    public function check_is_spam()
    {
        $faker = \Faker\Factory::create();

        $data = [
            'user_ip'                => $faker->ipv4,
            'user_agent'             => $faker->userAgent,
            'comment_author'         => 'viagra-test-123', //$faker->name,
            'comment_author_email'   => 'akismet-guaranteed-spam@example.com', //$faker->email,
            'comment_content'        => $faker->text(100),
            'comment_date_gmt'       => $faker->dateTimeAD->format('Y-m-d H:i:s'),
            'is_test'                => 1
        ];

        $spam = Akismet::check($data);

        $this->assertTrue($spam);
    }

    /** @test **/
    public function check_not_spam()
    {
        $faker = \Faker\Factory::create();

        $data = [
            'user_ip'                => $faker->ipv4,
            'user_agent'             => $faker->userAgent,
            'user_role'              => 'administrator',
            'comment_author'         => $faker->name,
            'comment_author_email'   => $faker->email,
            'comment_content'        => $faker->text(100),
            'comment_date_gmt'       => $faker->dateTimeAD->format('Y-m-d H:i:s'),
            'is_test'                => 1,

        ];

        $spam = Akismet::check($data);

        $this->assertFalse($spam);
    }

    /** @test **/
    public function submit_spam()
    {
        $faker = \Faker\Factory::create();

        $data = [
            'user_ip'                => $faker->ipv4,
            'user_agent'             => $faker->userAgent,
            'comment_author'         => 'viagra-test-123',
            'comment_author_email'   => 'akismet-guaranteed-spam@example.com',
            'comment_content'        => $faker->text(100),
            'comment_date_gmt'       => $faker->dateTimeAD->format('Y-m-d H:i:s'),
            'is_test'                => 1,
        ];

        $spam = Akismet::submitSpam($data);

        $this->assertTrue($spam);
    }

    /** @test **/
    public function submit_ham()
    {
        $faker = \Faker\Factory::create();

        $data = [
            'user_ip'                => $faker->ipv4,
            'user_agent'             => $faker->userAgent,
            'user_role'              => 'administrator',
            'comment_author'         => $faker->name,
            'comment_author_email'   => $faker->email,
            'comment_content'        => $faker->text(100),
            'comment_date_gmt'       => $faker->dateTimeAD->format('Y-m-d H:i:s'),
            'is_test'                => 1,
        ];

        $spam = Akismet::submitHam($data);

        $this->assertTrue($spam);
    }
}
